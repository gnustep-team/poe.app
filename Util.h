/*
    Util.h - Header for a class to manage the 'standard' vorbis
    comment fields for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#ifndef _UTIL_H_
#define _UTIL_H_

#include <Foundation/Foundation.h>

@interface Util: NSObject
{
  NSArray *_tags;
  NSArray *_tagsTitle;
  NSArray *_tagsDescription;
  NSMutableArray *_genres;
}

+ (id) singleInstance;

- (void) syncGenres: (NSNotification *)not;

- (NSArray *) tags;
- (NSArray *) tagsTitle;
- (NSArray *) tagsDescription;
- (NSArray *) genres;

- (NSString *) tagAtIndex: (int)index;
- (NSString *) titleAtIndex: (int)index;
- (NSString *) descriptionAtIndex: (int)index;

- (int) indexOfTag: (NSString *) tag;
- (int) indexOfTitle: (NSString *) title;

- (NSArray *) tagTemplate;
- (NSArray*) formatComments: (NSArray *)arrayIn;

@end

#endif // _UTIL_H_
