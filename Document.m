/*
    Document.m - NSDocument subclass for Poe.app
    Copyright (C) 2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#include "Document.h"
#include "OGGEditor.h"
#include "EditorWindowController.h"

@implementation Document

- (void) dealloc
{
  RELEASE(oggFile);
  [super dealloc];
}

- (void) addComment: (id) sender
{
  if([[[NSApp keyWindow] delegate] respondsToSelector: @selector(addComment:)])
    {
      [[[NSApp keyWindow] delegate] addComment: sender];
    }
}

- (void) deleteComment: (id) sender
{
	if([[[NSApp keyWindow] delegate] respondsToSelector: @selector(deleteComment:)])
	{
		[[[NSApp keyWindow] delegate] deleteComment: sender];
    }
}

// ***************************
// NSDocument Override methods
// ***************************

- (BOOL)readFromFile:(NSString *)fileName ofType:(NSString *)docType
{
	if ( [docType isEqualToString: @"Ogg/Vorbis File"] )
	{
		oggFile = [[OGGEditor alloc] initWithFile: fileName];
	}
	if (oggFile) return YES;
    return NO;
}

- (BOOL)writeToFile:(NSString *)fileName ofType:(NSString *)type
{
	if ([oggFile writeToFile: fileName]) return YES;
	return NO;
}

- (void) makeWindowControllers
{
	EditorWindowController *editor;

	editor = AUTORELEASE([[EditorWindowController alloc] initWithEditor: oggFile]);

	[self addWindowController: editor];
}

@end

