/*
    NSMutableArray+goodies. - additions to NSMutableArray for clever 
    placement within an array. for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#include "NSMutableArray+goodies.h" 

@implementation NSMutableArray (cleverPlacement)

- (void) insertObjectAlphabetically: (NSString *) anObject
{
  int i;

  for(i=0;i<[self count];i++)
    {
      if([anObject caseInsensitiveCompare: [self objectAtIndex: i]]
        == NSOrderedDescending)
        {
          continue;
        }
      else
        {
          break;
        }
    }

  [self insertObject: anObject atIndex: i];
}

@end

