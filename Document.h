/*
    Document.h - NSDocument subclass interface for Poe.app
    Copyright (C) 2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#ifndef __DOCUMENT_H__
#define __DOCUMENT_H__

#include <AppKit/NSDocument.h>
#include "OGGEditor.h"

@interface Document : NSDocument
{
	OGGEditor *oggFile;
}

- (void) addComment: (id) sender;
- (void) deleteComment: (id) sender;


// NSDocument Override methods
// ***************************

- (BOOL)readFromFile:(NSString *)fileName 
              ofType:(NSString *)docType;
- (BOOL)writeToFile:(NSString *)fileName 
             ofType:(NSString *)type;
- (void) makeWindowControllers;

@end

#endif // __DOCUMENT_H__
