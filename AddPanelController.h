/*
    AddPanelController.h - Header for the add comment panel controller
    for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#ifndef _ADDPANELCONTROLLER_H_
#define _ADDPANELCONTROLLER_H_

#include <AppKit/AppKit.h>
#include <Foundation/Foundation.h>
#include "Util.h"

@interface AddPanelController : NSWindowController
{
  IBOutlet NSBrowser *tagBrowser;
  IBOutlet NSButton *okButton;
  IBOutlet NSButton *cancelButton;
  IBOutlet NSTextField *label;

  NSMutableArray *_tags;
  NSString *_selection;
}

- (id) initWithPosition: (NSPoint) origin;
- (void) removeTag: (NSString *)tag;
- (void) okPressed: (id) sender;
- (void) cancelPressed: (id) sender;
- (NSString *) selection;

// NSBrowser delegate methods
//****************************

- (int)browser:(NSBrowser *)sender numberOfRowsInColumn:(int)column;

- (void)browser:(NSBrowser *)sender willDisplayCell:(id)cell
  atRow:(int)row column:(int)column;

@end

#endif // _ADDPANELCONTROLLER_H_
