Poe (A Pugnacious Ogg Editor)

Poe is a vorbis comment editor. It tries to follow the vorbis comment header
specification closely, while being convenient and flexible to use. Vorbis 
comment info can be found in the v-comment.html file. Field Names, and 
Implications are thepertinent parts. Towards that end, it doesn't have a 
static 'form' style interface. Instead, it has an editable table of comments. 
The contents of the table change dependent upon preference settings, and what 
comments are available in the ogg file you are editing.


Features
---------

* Allows multiple Artist, Performer, and Genre fields.
* Flexible choice of comment fields to edit.
* Allows editing of all the comment fields in a 
  file, not just the ones Poe is aware of.
* Localized for Thai, Japanese, German, Italian, and Bulgarian
  

Install
--------

In order to use Poe, you will need the following libraries installed 
on your system.

* GNUstep core libraries (http://www.gnustep.com/)
* libvorbis (http://www.xiph.org/)

Once you have those, cd to the Poe source directory and type:

make
make install (as root)


Usage
------

Preferences - select which comments you want to edit by default here. You
              can also add and remove genres here, although it won't have
              any effect on the editor just yet.

Editing - Delete comment will delete the currently selected comment. Add
          comment will give you a list of possible comments to add to the 
          file. I believe multiple occurances of all the comment fields
          are allowed, but for convenience sake, I've limited multiple
          occurances to just Artist, Performer, and Genre. I don't think
          it makes a lot of sense in the other cases.


Contributing
-------------

Take a look at ToDo.txt, Bugs.txt, and Contributing.txt 


License
--------

Poe is licensed under the GNU GPL 2.0. See License.txt. 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
